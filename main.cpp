#include <iostream>
#include "pessoa.hpp"
#include "aluno.hpp"
#include "professor.hpp"
#include <stdio.h>

using namespace std;

int main(int argc, char ** argv) {
   /*
   Pessoa pessoa_1;
   Pessoa pessoa_2("Maria", "555-1111", 21);

   Pessoa * pessoa_3;
   pessoa_3 = new Pessoa();//semelhante ao alloc;
 
   Pessoa * pessoa_4;
   pessoa_4 = new Pessoa("Marcelo", "666-7777", 25);   
  
   pessoa_1.setNome("Joao");
   pessoa_1.setMatricula("14/0078070");
   pessoa_1.setTelefone("555-5555");
   pessoa_1.setSexo("M");
   pessoa_1.setIdade(20);
   
   pessoa_3->setNome("Pateta");
   pessoa_3->setMatricula("10/12312313");
   pessoa_3->setTelefone("4444-1111");
   pessoa_3->setSexo("M");
   pessoa_3->setIdade(12);
   
   Aluno aluno_1;
   
   aluno_1.setNome("João");
   cout << "curso do aluno " << aluno_1.getNome() << ": " << aluno_1.getCurso() << endl;

   Professor professor_1;
	professor_1.imprimeDadosProfessor();   

   
   //cout << "Nome: " << pessoa_1.nome; << endl;
   cout << "Nome: " << pessoa_1.getNome() << endl;
   cout << "Matrícula: " << pessoa_1.getMatricula() << endl;
   cout << "Telefone: " << pessoa_1.getTelefone() << endl;
   cout << "Sexo: " << pessoa_1.getSexo() << endl;
   cout << "Idade: " << pessoa_1.getIdade() << endl;

   cout << endl;
   cout << "Nome: " << pessoa_2.getNome() << endl;
   cout << "Telefone: " << pessoa_2.getTelefone() << endl;
   cout << "Idade: " << pessoa_2.getIdade() << endl;
   
   
   pessoa_1.imprimeDados();
   pessoa_2.imprimeDados();
   pessoa_3->imprimeDados();
   pessoa_4->imprimeDados();
    

   // Lista de Alunos
   Aluno listaDeAlunos[20];

   

   delete(pessoa_3);
   delete(pessoa_4);//equivale ao free;
   */
  
   // Lista de Alunos
   Aluno *lista_de_alunos[20];
  
   string nome;   
   string matricula;
   int idade;
   string sexo; 
   string telefone; 
   float ira; 
   int semestre; 
   string curso; 
  
   int decisao, qtdAlunos=0, i;
   for(i = 0; i<20 ; i++){
      cout << "DIGITE 1 PARA ADICIONAR UM NOVO ALUNO OU 0 PARA SAIR: " << endl;
      cin >> decisao;
   
      if(decisao == 0){break;}

      // Entrada de dados do terminal (stdin) 
      cout << "Nome: "; 
      cin >> nome;
      cout << "Matricula: "; 
      cin >> matricula;  
      cout << "Idade: "; 
      cin >> idade; 
      cout << "Sexo: ";
      cin >> sexo;
      cout << "Telefone: ";
      cin >> telefone;
      cout << "IRA: ";
      cin >> ira;
      cout << "Semestre: ";
      cin >> semestre;
      cout << "Curso: ";
      cin >> curso;

      // Criação do objeto da lista 
      lista_de_alunos[i] = new Aluno();   
 
      // Definição dos atributos do objeto  
      lista_de_alunos[i]->setNome(nome); 
      lista_de_alunos[i]->setMatricula(matricula); 
      lista_de_alunos[i]->setIdade(idade); 
      lista_de_alunos[i]->setSexo(sexo); 
      lista_de_alunos[i]->setTelefone(telefone);
      lista_de_alunos[i]->setIra(ira);
      lista_de_alunos[i]->setSemestre(semestre);
      lista_de_alunos[i]->setCurso(curso);

      qtdAlunos++;
   }
   
   for(i = 0 ; i<qtdAlunos; i++){
      // Impressão dos atributos do objeto
      lista_de_alunos[i]->imprimeDadosAluno();
   }
   for(i=0 ; i<qtdAlunos ; i++){
      delete(lista_de_alunos[i]);
   }

   return 0;
}












