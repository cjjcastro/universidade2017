#include "professor.hpp"
#include <iostream>

using namespace std;

Professor::Professor(){
	setNome("");
	setMatricula("");
	setIdade(0);
	setSexo("");
	setTelefone("");
	setFormacao("");
	setSalario(0.0);
	setSala(0);

}
Professor::~Professor(){}

void Professor::setFormacao(string formacao){
	this->formacao = formacao;

}
string Professor::getFormacao(){
	return formacao;

}
void Professor::setSalario(float salario){
	this->salario = salario;

}
float Professor::getSalario(){
	return salario;

}
void Professor::setSala(int sala){
	this->sala = sala;

}
int Professor::getSala(){
	return sala;

}

void Professor::imprimeDadosProfessor(){
	cout << "dados professor: " << endl;
	imprimeDados();
	cout << "Formaçao: " << getFormacao() << endl;
	cout << "Salário: " << getSalario() << endl;
	cout << "Sala: " << getSala() << endl;

}
